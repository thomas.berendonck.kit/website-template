module.exports = {
    title: 'Advanced Git & Gitlab',
    description: 'An interactive course where you get to publish your own website',
    base: '/',
    dest: 'public',
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Alumni', link: '/alumni/' },
        ]
    }
}
