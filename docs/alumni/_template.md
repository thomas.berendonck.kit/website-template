# Alumni: `<Username>`

![Random placeholder image](https://picsum.photos/640/360)

*Open the [URL of the image: https://picsum.photos/640/360](https://picsum.photos/640/360) in your browser and use the resulting slightly longer URL instead of the short one, to prevent it from changing. Then remove this note.*

## About me

I've been using git for <time> and I'm interested in <interests>.

The technologies I typically work with are <technology> and <technology>.
